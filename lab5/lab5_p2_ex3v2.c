#include <stdio.h>

int main()
{
	int k = 0, i = 0, j = 0;
	int imax = 100000;

/*	for (i = 0; i < imax; i++) {
		for (j = 0; j < imax; j++) {
			k++;
		}
	}*/

	__asm {
		mov eax, 0
		mov edx, 100000 

	loop:
		mov ebx, 0 // j = 0
		cmp eax, edx // c = i - max
		jz fim // c == 0
		inc eax // i++
	innerLoop:
		cmp ebx, edx // c = j - max
		jz loop // c == 0
		inc ebx // j++

		inc ecx // k++
		jmp innerLoop
	fim:
		mov k, ecx
	}

	printf("Resultado: %d\n", k);
	return 0;
}
