#include <stdlib.h>
#include <math.h>

int main()
{
	int i, j, k, N, norm;
	double *A, *B, *C;

	N = 4000;
	A = (double *) malloc(sizeof(double) * N * N);
	B = (double *) malloc(sizeof(double) * N * N);
	C = (double *) malloc(sizeof(double) * N * N);

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			A[i * N + j] = (double) random();
		}
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			B[i * N + j] = (double) random();
		}
	}

	for (j = 0; j < N; j++) {
		for (i = 0; i < N; i++) {
			for (k = 0; k < N; k++) {
				C[i * N + j] += A[i * N + k] * B[k * N + j];
			}
		}
	}

	return 0;
}
