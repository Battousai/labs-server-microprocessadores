#include <stdlib.h>
#include <math.h>

int main()
{
	int i, j, k, N, norm;
	double *A, *B, *C;

	N = 4000;
	A = (double *) malloc(sizeof(double) * N);
	B = (double *) malloc(sizeof(double) * N);
	C = (double *) malloc(sizeof(double) * N);

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			A[i + j] = (double) random();
		}
	}

	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			B[i + j] = (double) random();
		}
	}

	for (j = 0; j < N; j++) {
		for (i = 0; i < N; i++) {
			C[j + i] += A[j + i] * B[j + i];
		}
	}

	return 0;
}
