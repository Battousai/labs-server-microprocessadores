#include <mpi.h>

int main(int argc, char** argv)
{
	int men[8], nprocs, myrank, send, i;
	const int root = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	send = myrank + 1;
	MPI_Gather(	&send, 1, MPI_INTEGER, 	// enviar para todos
			men, 1, MPI_INTEGER,	// recebe de cada proc ordenado pelo rank
			root, MPI_COMM_WORLD);	// processo que envia é o root,
						// Comunica com todos

	if (root == myrank) {
		printf("Resultado: %d %d %d %d %d %d %d %d\n",
				men[0], men[1], men[2], men[3], men[4], men[5], men[6], men[7]);
	}
	MPI_Finalize();
	return 0;
}
