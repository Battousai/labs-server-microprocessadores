#include <mpi.h>
#include <stdio.h>

/*
 * MPI_Reduce(
 * 	void* send_data,
 * 	void* recv_data,
 * 	int count,
 * 	MPI_Datatype datatype,
 * 	MPI_Op op,
 * 	int root,
 * 	MPI_Comm communicator)
 */

int main(int argc, char** argv)
{
	int nprocs, myrank, inicio, final, i;
	float a[1000], soma, tmp;
	const int root = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	soma = 0.0;
	inicio = myrank * 1000 / nprocs + 1;
	final = inicio + 1000 / nprocs - 1;

	for (i = inicio; i <= final; i++) {
		a[i] = 1.0;
	}
	for (i = inicio; i <= final; i++) {
		soma += a[i];
	}

	MPI_Reduce(	&soma, // dados que processos querem reduzir 
			&tmp, // recebe resultado aqui
			1, // sizeof(datatype) * count
			MPI_REAL, // sizeof(MPI_REAL) * count
			MPI_SUM, // operação
			root, // processo root
			MPI_COMM_WORLD);

	if (myrank == root) {
		printf("Soma: %f\n", tmp);
	}
	MPI_Finalize();
	return 0;
}
