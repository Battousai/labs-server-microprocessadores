#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>

int main(int argc, char** argv)
{
	int inicio, final, i, j, n;
	double *x, *y;
	double som, h, total;
	const int root = 0;

	MPI_Init(&argc, &argv);
	int nprocs;
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	int myrank;
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	n = 200000 / nprocs;
	x = (double *) malloc(sizeof(double) * n);
	y = (double *) malloc(sizeof(double) * n);

	inicio = myrank * n / nprocs + 1;
	final = inicio + n / nprocs - 1;

	h = 20.0 / n;
	for (i = inicio; i <= final; i++) {
		x[i] = -10.0 + 20.0 * (i - 1) / (n - 1);
	}
	
	for (i = 0; i < n; i++) {
		y[i] = -10.0 + 20.0 * (i - 1) / (n - 1);
	}

	som = 0.0;
	for (i = inicio; i <= final; i++) {
		for (j = 0; j < n; j++) {
			som += exp(-0.5 * (x[i] * x[i] + y[j] * y[j]));
		}
	}

	MPI_Reduce(	&som,
			&total,
			1,
			MPI_DOUBLE,
			MPI_SUM,
			root,
			MPI_COMM_WORLD);


	if (root == myrank) {
		total = total * h * h;
		printf("Soma: %f\n", total);
	}
	MPI_Finalize();
	return 0;
}
