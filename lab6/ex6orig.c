#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv)
{
	int inicio, final, i, j, n;
	double *x, *y;
	double som, h;

	n = 200000;
	x = (double *) malloc(sizeof(double) * n);
	y = (double *) malloc(sizeof(double) * n);

	h = 20.0 / n;
	for (i = 0; i < n; i++) {
		x[i] = -10.0 + 20.0 * (i - 1) / (n - 1);
		y[i] = -10.0 + 20.0 * (i - 1) / (n - 1);
	}

	som = 0.0;
	for (i = 0; i < n; i++) {
		for (j = 0; j < n; j++) {
			som += exp(-0.5 * (x[i] * x[i] + y[j] * y[j]));
		}
	}

	som = som * h * h;
	printf("Soma: %f\n", som);
	return 0;
}
