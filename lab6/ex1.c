#include "mpi.h"

int main(int argc, char *argv[])
{
	int myrank;
	int nprocs;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs); // Número de processos (processos para o trabalho)
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank); // Rank (identificação)
	
	printf("nprocs=%d My rank=%d\n", nprocs, myrank);
	MPI_Finalize();
	return 0;
}
