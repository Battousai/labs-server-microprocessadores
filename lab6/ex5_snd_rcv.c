#include <mpi.h>

/*
 * MPI_Send(
 * 	void* data, // data buffer
 * 	int count, // data count
 * 	MPI_Datatype datatype, // data type
 * 	int destination, // rank destination
 * 	int tag, // channel
 * 	MPI_Comm communicator)
 */
/**
 * MPI_Recv(
 * 	void* data,
 * 	int count, // receive at most
 * 	MPI_Datatype datatype,
 * 	int source,
 * 	int tag,
 * 	MPI_Comm communicator,
 * 	MPI_Status* status)
 */

int main(int argc, char** argv)
{
	int nprocs, myrank, i;
	int valor, novo_valor, soma;
	int left, right, tag;

	MPI_Status status;
	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	tag = 205;
	right = (myrank + nprocs + 1) % nprocs;
	left = (myrank + nprocs - 1) % nprocs;
	soma = 0;
	valor = myrank;
	novo_valor = 0;

	for (i = 0; i < nprocs; i++) {
		// envia o meu rank para o processo à minha direita
		MPI_Send(&valor, 1, MPI_INTEGER, right, tag, MPI_COMM_WORLD);
		// recebe o rank do processo à minha esquerda
		MPI_Recv(&novo_valor, 1, MPI_INTEGER, left, tag, MPI_COMM_WORLD, &status);
		soma += novo_valor;
		valor = novo_valor;
		printf("Soma parcial: %d, Processo: %d\n", soma, myrank);
	}

	if (myrank == 0) {
		printf("Resultado final: %d\n", soma);
	}
	MPI_Finalize();
	return 0;
}
