#include <mpi.h>
#include <stdio.h>

int main(int argc, char *argv[])
{
	int men[4], nprocs, myrank, i;
	const int root = 0;

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &nprocs);
	MPI_Comm_rank(MPI_COMM_WORLD, &myrank);

	if (root == myrank) {
		for (i = 0; i < 4; i++) {
			men[i] = i;
		}
	} else {
		for (i = 0; i < 4; i++) {
			men[i] = 0;
		}
	}

	printf("[r:%d] Antes: %d %d %d %d\n", myrank, men[0], men[1], men[2], men[3]);
	MPI_Bcast(men, 4, MPI_INTEGER, root, MPI_COMM_WORLD);
	printf("[r:%d] Depois: %d %d %d %d\n", myrank, men[0], men[1], men[2], men[3]);
	MPI_Finalize();
	return 0;
}
